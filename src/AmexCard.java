
public class AmexCard implements IChain{
	
	private IChain nextChain;
	
	@Override
	public void setNextChain(IChain nextChain) {
		this.nextChain = nextChain;
		
	}

	@Override
	public String type(Card card) {
		if("Amex".equals(card.getTypeOfCard()) ) {
			return "Amex";
		}
		
		return "";
	}
	
	
}
