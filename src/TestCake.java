import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class TestCake {

	
	static IChain chainStep1 = new VisaCard();
	static IChain chainStep2 = new GoogleCard();
	static IChain chainStep3 = new AmexCard();
	
	
	@BeforeAll
	public static void setup() {
		chainStep1.setNextChain(chainStep2);
		chainStep2.setNextChain(chainStep3);
	}
	
	
	@ParameterizedTest
	@MethodSource("provideIngredients")
	void Chaintest(String tipeOfCard, Integer amount, String expect) {
		Card request = new Card(tipeOfCard,amount);
		
		assertTrue(chainStep1.type(request) == expect);
		
	}
	
	private static Stream<Arguments> provideIngredients(){
		return Stream.of(
					Arguments.of("Visa",5000,"Visa"),	
					Arguments.of("Amex", 15000, "Amex"),
					Arguments.of("Google",2000, "Google")
				);
	}


}
