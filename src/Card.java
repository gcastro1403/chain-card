
public class Card {
	
	private String typeOfCard;
	private Integer amount;
	
	
	public Card(String typeOfCard,Integer amount) {
		this.typeOfCard = typeOfCard;
		this.amount = amount;
	}


	public String getTypeOfCard() {
		return typeOfCard;
	}


	

	public Integer getAmount() {
		return amount;
	}


	
}
