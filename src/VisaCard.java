
public class VisaCard implements IChain{
	
	private IChain nextChain;
	
	@Override
	public void setNextChain(IChain nextChain) {
		this.nextChain = nextChain;
		
	}

	@Override
	public String type(Card card) {
		if("Visa".equals(card.getTypeOfCard())) {
			return "Visa";
		}
		return this.nextChain.type(card);
	}

}
