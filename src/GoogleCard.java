
public class GoogleCard implements IChain{
	
	private IChain nextChain;

	@Override
	public void setNextChain(IChain nextChain) {
		
		this.nextChain = nextChain;
		
	}
	

	@Override
	public String type(Card card) {
		if("Google".equals(card.getTypeOfCard())) {
			return "Google";
			 
		}
		return this.nextChain.type(card);
		
	}
	
	
}
